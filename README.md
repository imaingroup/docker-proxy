# Docker.
## Requirements.

Linux. 

Docker 20.10.2+. 

Docker compose 1.25.0+.

## Install soft.
```
    sudo apt update && apt upgrade
    sudo apt install docker docker.io containerd docker-compose git zip
```

## Get docker.
```
    mkdir -p /var/docker/imaingroup && /var/docker/imaingroup
    git clone https://gitlab+deploy-token-590961@gitlab.com/imaingroup/docker-proxy.git
```

##	Production environment: before the first launch.
```
    cd /var/docker/imaingroup
    
    mkdir -p secrets/main/ssl/nginx
    scp /you/file /var/docker/imaingroup/secrets/main/ssl/nginx/ssl.crt
    scp /you/file /var/docker/imaingroup/secrets/main/ssl/nginx/ssl.key
```

##	Production deploy.
```
    cd docker
    sudo docker-compose -f build.yml build
    sudo docker stack deploy -c stack.yml proxy
    
    #test
    sudo docker-compose -f stack.yml up
```
